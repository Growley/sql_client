unit MainWindow;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SQLDB, SQLite3Conn, mysql57conn, PQConnection, DB,
  IBConnection, Forms, Controls, Graphics, Dialogs, DBGrids, DBCtrls, StdCtrls,
  Buttons, ExtCtrls, StrUtils;

type

  { TMainForm }

  TMainForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    DBTableName: TComboBox;
    DBtblNameLbl: TLabel;
    Edit1: TEdit;
    DBSelDial: TOpenDialog;
    DBNameBox: TLabeledEdit;
    LeftSplit: TSplitter;
    BottomSplit: TSplitter;
    RadioGroup1: TRadioGroup;
    SelectDBBtn: TBitBtn;
    ConnectBtn: TButton;
    DisconBtn: TButton;
    SQLComExec: TButton;
    SQLQuery1: TSQLQuery;
    SQLQuery1DATA: TDateField;
    SQLQuery1FIO: TStringField;
    SQLQuery1PATIENT: TSmallintField;
    SQLQuery2: TSQLQuery;
    UpdateBtn: TButton;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    SQLCommB: TEdit;
    IBConnection1: TIBConnection;
    SQLComLbl: TLabel;
    RightSplit: TSplitter;
    TopSplit: TSplitter;
    SQLDBPath: TLabeledEdit;
    LoginEdBox: TLabeledEdit;
    PasswEdBox: TLabeledEdit;
    SQLCommCls: TSpeedButton;
    SQLTransaction1: TSQLTransaction;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ConnectBtnClick(Sender: TObject);
    procedure DBSelBoxChange(Sender: TObject);
    procedure DBTableNameChange(Sender: TObject);
    procedure DisconBtnClick(Sender: TObject);
    procedure LoginEdBoxChange(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure SQLComExecClick(Sender: TObject);
    procedure SQLDBPathChange(Sender: TObject);
    procedure TopSplitCanOffset(Sender: TObject; var NewOffset: Integer;
      var Accept: Boolean);
    procedure UpdateBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OpTabClick(Sender: TObject);
    procedure SelectDBBtnClick(Sender: TObject);
    procedure SQLCommClsClick(Sender: TObject);
    procedure SQLOp();
    procedure SQLOT();
    procedure SQLCommit();
    procedure DisableSomeBtns();
    procedure SQLConsole();
  private

  public

  end;

var
  MainForm: TMainForm;
  active_t: boolean;
  connect_flag: boolean = false;
  open_table: string;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.SQLOT();
begin
try
    SQLQuery1.Active:=false;
    SQLQuery1.sql.Clear;
    SQLQuery1.sql.add('select * from ' + DBTableName.Text);
    SQLQuery1.Open;
  except
    ShowMessage('Cannot execute SQL Querry');
  end;
 end;

procedure TMainForm.SQLOp();
begin
  IBConnection1.UserName:=LoginEdBox.Text;
  IBConnection1.Password:=PasswEdBox.Text;
  IBConnection1.DatabaseName:=SQLDBPath.Text;

  try
    IBConnection1.Connected:=True;
    SQLTransaction1.DataBase:=IBConnection1;
    SQLQuery1.DataBase:=IBConnection1;
    SQLQuery2.DataBase:=IBConnection1;
    DBTableName.Clear;
    try
      IBConnection1.GetTableNames(DBTableName.Items,false);
    except
      IBConnection1.GetTableNames(DBTableName.Items,true);
    end;
    DBTableName.ItemIndex:=0;
    except
      ShowMessage('DEBUG_CONNECTION_FAILED');
    end;
  try
    SQLTransaction1.Active:=true;
  except
    ShowMessage('DEBUG_TRANSACTION_FAILED');
  end;
  try
    SQLQuery1.Active:=false;
    SQLQuery1.sql.Clear;
    connect_flag:=true
  except
    ShowMessage('DEBUG_SQL_CALL_FAILED');
  end;
end;


procedure TMainForm.ConnectBtnClick(Sender: TObject);
begin
  SQLOp();
  SQLOT();
  DisableSomeBtns();
  DBGrid1.Visible:=true;
  DBNavigator1.Visible:=true;
  UpdateBtn.Visible:=true;
end;

procedure TMainForm.Button1Click(Sender: TObject);
var s:string;
begin
  SQLQuery1.Active:=false;
  SQLQuery1.SQL.Clear;
  s:=edit1.Text;
  SQLQuery1.sql.add('SELECT * from  GROWLEY where fio like '+'''%' +s+ '%'+''''+' ;');
  SQLQuery1.Open;
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  edit1.Clear;
  SQLQuery1.Active:=false;
  SQLQuery1.SQL.Clear;
  SQLQuery1.sql.add('SELECT * from  GROWLEY  ;');
  SQLQuery1.Open;
  SQLOT();
end;

procedure TMainForm.DBSelBoxChange(Sender: TObject);
  begin
  SQLDBPath.Visible:=True;
  SelectDBBtn.Visible:=True;
  DBNameBox.Visible:=False;
  end;

procedure TMainForm.DBTableNameChange(Sender: TObject);
begin
  try
    SQLQuery1.Active:=false;
    SQLQuery1.sql.Clear;
    SQLQuery1.sql.add('select * from ' + DBTableName.Text);
    SQLQuery1.Open;
  except
    ShowMessage('Cannot execute SQL Querry');
  end;
end;

procedure TMainForm.DisconBtnClick(Sender: TObject);
begin
  SQLCommit();
  connect_flag:=false;
  DisableSomeBtns();
  DBGrid1.Visible:=false;
  DBNavigator1.Visible:=false;
  UpdateBtn.Visible:=false;
  IBConnection1.Connected:=false;
  IBConnection1.UserName:='';
  IBConnection1.Password:='';
  IBConnection1.DatabaseName:='';
  RadioGroup1.Visible:=False;
  Edit1.Visible:=False;
  Button1.Visible:=False;
  Button2.Visible:=False;
end;

procedure TMainForm.LoginEdBoxChange(Sender: TObject);
begin

end;

procedure TMainForm.RadioGroup1Click(Sender: TObject);
var s:string;
begin
    s:='';
     case radiogroup1.ItemIndex of
     0: s:='data';
     1: s:='fio';
     2: s:='patient';
     end;
    try
      SQLQuery1.Active:=false;
      SQLQuery1.SQL.Clear;
      SQLQuery1.sql.add('SELECT * FROM GROWLEY order by '+ s);
      SQLQuery1.Open;
    except
      ShowMessage(' Ошибка при выполнении SQL запроса к таблице с клиентами.');
    exit;
    end;

end;

procedure TMainForm.SQLComExecClick(Sender: TObject);
begin
  SQLConsole();
end;

procedure TMainForm.SQLDBPathChange(Sender: TObject);
begin
  SQLOp();
  SQLOT();
  DisableSomeBtns();
  DBGrid1.Visible:=true;
  DBNavigator1.Visible:=true;
  UpdateBtn.Visible:=true;
end;

procedure TMainForm.TopSplitCanOffset(Sender: TObject; var NewOffset: Integer;
  var Accept: Boolean);
begin

end;

procedure TMainForm.UpdateBtnClick(Sender: TObject);   // Требуем подтверждение транзакции
begin
  if MessageDlg('Транзакция','Вы хотите подтвердить или откатить транзакцию?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
  try
  SQLQuery1.ApplyUpdates;
  SQLTransaction1.Commit;
  except
    ShowMessage('DEBUG_SQL_COMMIT_TRANSACTION_FAILED');
  end
  else
  try
  SQLTransaction1.Rollback;
  except
    ShowMessage('DEBUG_SQL_ROLLBACK_FAILED');
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Active_t:=false;
end;

procedure TMainForm.OpTabClick(Sender: TObject);
begin
  try
    SQLQuery1.Active:=false;
    SQLQuery1.sql.Clear;
    SQLQuery1.sql.add('select * from ' + DBTableName.Text);
    SQLQuery1.Open;
  except
    ShowMessage('Cannot execute SQL Querry');
  end;
end;

procedure TMainForm.SelectDBBtnClick(Sender: TObject);
begin
  if DBSelDial.Execute then SQLDBPath.Text:=DBSelDial.FileName;
end;

procedure TMainForm.SQLCommClsClick(Sender: TObject);
begin
  SQLCommB.Text:='';
end;

procedure TMainForm.SQLCommit();
begin
  SQLQuery1.Active:=false;
  SQLTransaction1.Commit;
  IBConnection1.Connected:=false;
  SQLTransaction1.Active:=false;
  connect_flag:=false;
  DisableSomeBtns();
end;

procedure TMainForm.DisableSomeBtns();
begin
  if connect_flag = true then begin
  // Отключаем кнопки стартового экрана
  LoginEdBox.Enabled:=False;
  PasswEdBox.Enabled:=False;
  DBNameBox.Enabled:=False;
  ConnectBtn.Enabled:=false;
  SQLDBPath.Enabled:=False;
  RadioGroup1.Visible:=True;
  Edit1.Visible:=True;
  Button1.Visible:=True;
  Button2.Visible:=True;
  // И включаем другие
  DisconBtn.Enabled:=true;
  DBTableName.Enabled:=true;
  SQLComLbl.Visible:=true;
  SQLCommB.Visible:=true;
  SQLComExec.Visible:=true;
  SQLCommCls.Visible:=true;
  UpdateBtn.Enabled:=true;
  end
  else begin
  // Включаем кнопки стартового экрана
  LoginEdBox.Enabled:=True;
  PasswEdBox.Enabled:=True;
  SQLDBPath.Enabled:=True;
  ConnectBtn.Enabled:=true;
  DBNameBox.Enabled:=true;
  DisconBtn.Enabled:=false;
  DBTableName.enabled:=false;
  SQLComLbl.Visible:=false;
  SQLCommB.Visible:=false;
  SQLComExec.Visible:=false;
  SQLCommCls.Visible:=false;
  UpdateBtn.Enabled:=false;
  SQLDBPath.Enabled:=True;
end;
end;

procedure TMainForm.SQLConsole();
begin
  try
  if AnsiStartsText('insert', SQLCommB.Text)=true then begin
  SQLQuery2.Active:=false;
  SQLQuery2.SQL.Clear;
  SQLQuery2.SQL.Add(SQLCommB.Text);
  SQLQuery2.ExecSQL;
  end
  else if AnsiStartsText('delete', SQLCommB.Text)=true then begin
  SQLQuery2.Active:=false;
  SQLQuery2.SQL.Clear;
  SQLQuery2.SQL.Add(SQLCommB.Text);
  SQLQuery2.ExecSQL;
  end
  else if AnsiStartsText('update', SQLCommB.Text)=true then begin
  SQLQuery2.Active:=false;
  SQLQuery2.SQL.Clear;
  SQLQuery2.SQL.Add(SQLCommB.Text);
  SQLQuery2.ExecSQL;
  end
  else if AnsiStartsText('select', SQLCommB.Text)=true then begin
  SQLQuery1.Active:=false;
  SQLQuery1.SQL.Clear;
  SQLQuery1.SQL.Add(SQLCommB.Text);
  SQLQuery1.Open;
  end;
  except
    ShowMessage('DEBUG_SQL_CMD_ERROR');
  end;
end;

end.
